/* rsa-pss-sign-tr.c

   PSS signatures.

   Copyright (C) 2015 Andrew James Lawrence

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include "rsa.h"
#include "pss.h"
#include "gmp-glue.h"

int
rsa_pss_sign_tr(const struct rsa_public_key *pub,
  	          const struct rsa_private_key *key,
	          void *random_ctx, nettle_random_func *random,
	          void* hash_ctx, const struct nettle_hash *hash_func,
              size_t salt_length,
              size_t m_hash_length, const uint8_t *m_hash,
              mpz_t signature)
{  
	mpz_t em;
	int ret;
	mpz_init(em);

    TMP_GMP_DECL(salt, uint8_t);
	TMP_GMP_ALLOC(salt, salt_length);

	random(random_ctx, salt_length, salt);

	ret = emsapss_rsa_encode(hash_ctx, hash_func,
		     	             salt_length, salt,
			     	         m_hash_length, m_hash,
	                         key->size * 8, em)
	 && rsa_compute_root_tr (pub, key, random_ctx, random,
	      						 signature, em);
	mpz_clear(em);
	TMP_GMP_FREE(salt);
	return ret;
}
