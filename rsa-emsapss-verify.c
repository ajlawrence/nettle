/* rsa-emsapss-verify.c

   Verification primitive for PSS signatures.

   Copyright (C) 2015 Andrew James Lawrence

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include "pss.h"
#include "bignum.h"
#include "gmp-glue.h"
#include "nettle-internal.h"

int
emsapss_rsa_verify(void* hash_ctx, const struct nettle_hash *hash_func,
                   size_t salt_length,
                   size_t m_hash_length, const uint8_t *m_hash,
                   size_t embits, const mpz_t m)
{
	/* The numbered items in comments are from the RFC description    */
    /*	   1.  If the length of M is greater than the input limitation for the
	       hash function (2^61 - 1 octets for SHA-1), output "inconsistent"
	       and stop.

	   2.  Let mHash = Hash(M), an octet string of length hLen. */
	/* m_hash is an input to this function so we can skip the above two steps */
	int ret;
    size_t i;

	const size_t emlen = (embits + 7) / 8;
	const size_t db_length = emlen - m_hash_length - 1;

	// Convert m to an octet string em of length emlen
	TMP_GMP_DECL(em, uint8_t);
    TMP_GMP_DECL(db, uint8_t);
    TMP_GMP_DECL(h, uint8_t);
    TMP_GMP_DECL(h_prime, uint8_t);
    TMP_GMP_DECL(salt, uint8_t);
    TMP_GMP_DECL(m_prime, uint8_t);

	TMP_GMP_ALLOC(em, emlen);
	TMP_GMP_ALLOC(db, db_length);
	TMP_GMP_ALLOC(h, m_hash_length);
	TMP_GMP_ALLOC(h_prime, m_hash_length);
	TMP_GMP_ALLOC(salt, salt_length);
	TMP_GMP_ALLOC(m_prime, 8 + m_hash_length + salt_length);

	nettle_mpz_get_str_256(emlen, em, m);

    /* 3.  If emLen < hLen + sLen + 2, output "inconsistent" and stop. */
    if (emlen < m_hash_length + salt_length +2)
    {
    	ret = 0;
    	goto cleanup;
    }

    /*  4.  If the rightmost octet of EM does not have hexadecimal value
        0xbc, output "inconsistent" and stop. */
    if (em[emlen - 1] != 0xbc)
    {
    	ret = 0;
    	goto cleanup;
    }

    /* 5.  Let maskedDB be the leftmost emLen - hLen - 1 octets of EM, and
        let H be the next hLen octets. */
	memcpy(db, em, db_length);
	memcpy(h, em + db_length, m_hash_length);

	/*   6.  If the leftmost 8emLen - emBits bits of the leftmost octet in
	       maskedDB are not all equal to zero, output "inconsistent" and
	       stop. */
	if ((db[0] & (0xff << (unsigned int)(8 - (8 * emlen-embits)))) != 0)
    {
    	ret = 0;
    	goto cleanup;
    }

	/* 7.  Let dbMask = MGF(H, emLen - hLen - 1).
	   8.  Let DB = maskedDB \xor dbMask. */
	if (!_nettle_mgf1xor(hash_ctx, hash_func, m_hash_length, h, db_length, db))
    {
    	ret = 0;
    	goto cleanup;
    }

	/* 9.  Set the leftmost 8emLen - emBits bits of the leftmost octet in DB
	       to zero. */
    db[0] &= (0xFF >> (unsigned int)(8*emlen-embits));

    /* 10. If the emLen - hLen - sLen - 2 leftmost octets of DB are not zero
        or if the octet at position emLen - hLen - sLen - 1 (the leftmost
        position is "position 1") does not have hexadecimal value 0x01,
        output "inconsistent" and stop. */
    for (i = 0; i < db_length-2; i++)
    {
    	if (db[i] !=  0x00)
    	{
        	ret = 0;
        	goto cleanup;
    	}
    }

    if (db[db_length - 2] != 0x01)
    {
    	ret = 0;
    	goto cleanup;
    }

    /* 11.  Let salt be the last sLen octets of DB. */
	memcpy(salt, db + (db_length - salt_length), salt_length);

	/*  12.  Let M' = (0x)00 00 00 00 00 00 00 00 || mHash || salt ;
	         M' is an octet string of length 8 + hLen + sLen with eight
	         initial zero octets. */
	// Set the inital octets to be 0
	memset(m_prime, 0, 8);

	// copy m_hash into m_prime
	memcpy(m_prime + 8, m_hash, m_hash_length);

	// copy the salt into m_prime after the hash
	memcpy(m_prime + 8 + m_hash_length, salt, salt_length);

	/*  13. Let H' = Hash(M'), an octet string of length hLen. */
	hash_func->init(hash_ctx);
    hash_func->update(hash_ctx, 8 + m_hash_length + salt_length, m_prime);
    hash_func->digest(hash_ctx, m_hash_length, h_prime);

	/*  14. If H = H', output "consistent." Otherwise, output "inconsistent." */
    for (i = 0; i < m_hash_length; i++)
    {
    	if (h_prime[i] != h[i])
    	{
    		ret = 0;
    		goto cleanup;
    	}
    }

    ret = 1;
cleanup:
	TMP_GMP_FREE(em);
	TMP_GMP_FREE(db);
	TMP_GMP_FREE(h);
	TMP_GMP_FREE(salt);
	TMP_GMP_FREE(m_prime);
	TMP_GMP_FREE(h_prime);
	return ret;
}

