/* pss.h

   PSS embedding.

   Copyright (C) 2015 Andrew James Lawrence

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#ifndef NETTLE_PSS_H_INCLUDED
#define NETTLE_PSS_H_INCLUDED

#include "nettle-types.h"
#include "bignum.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Name mangling */
#define emsapss_rsa_encode nettle_emsapss_rsa_encode
#define emsapss_rsa_verify nettle_emsapss_rsa_verify
#define _increment_count nettle_increment_count

/* PSS primitive functions */
int
emsapss_rsa_encode(void* hash_ctx, const struct nettle_hash *hash_func,
		           size_t salt_length, const uint8_t *salt,
			       size_t m_hash_length, const uint8_t *m_hash,
                   size_t embits, mpz_t em);

int
emsapss_rsa_verify(void* hash_ctx, const struct nettle_hash *hash_func,
                   size_t salt_length,
                   size_t m_hash_length, const uint8_t *m_hash,
                   size_t embits, const mpz_t em);

// Internal mask generation functions
int _nettle_mgf1xor(void* hash_ctx, const struct nettle_hash *hash_func,
		 size_t m_hash_length, const uint8_t *h,
		 size_t db_length, uint8_t *db);

void _increment_count(uint8_t *c);
/*  Mask generation function constants */
#define MGF1_SIZE 32

#ifdef __cplusplus
}
#endif

#endif /* NETTLE_PSS_H_INCLUDED */
