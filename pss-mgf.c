/* pss-mgf.c

   Mask generation function for the RSA public key algorithm. PSS encryption.

   Copyright (C) 2015 Andrew James Lawrence

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "pss.h"
#include "gmp-glue.h"
#include "memxor.h"
#include "macros.h"

int
_nettle_mgf1xor(void* hash_ctx, const struct nettle_hash *hash_func,
		    size_t m_hash_length, const uint8_t *h_seed,
		    size_t db_length, uint8_t *db)
{
	int ret;
	size_t i;

	TMP_GMP_DECL(t, uint8_t);
	TMP_GMP_DECL(hash_input, uint8_t);

	TMP_GMP_ALLOC(hash_input, m_hash_length + 4);
	TMP_GMP_ALLOC(t, m_hash_length);

	// 1. If maskLen > 2^32 hLen, output "mask too long" and stop.
	// Is this necessary? Is it the most efficient way to perform the check?
	if (db_length > 0 && ((db_length - 1) >> 32) >= m_hash_length)
	{
		ret = 0;
		goto cleanup;
	}

	// 2. Let T be the empty octet string.
	// Done above

    /* 3. For counter from 0 to \ceil (maskLen / hLen) - 1, do the
	      following:
	      a. Convert counter to an octet string C of length 4 octets (see
	         Section 4.1):
	         	 C = I2OSP (counter, 4) .
	      b. Concatenate the hash of the seed mgfSeed and C to the octet
	         string T:
	         	 T = T || Hash(mgfSeed || C) . */

	// We need an octet string to pass as input to the hash function
	// hash_input = mgfseed || C
	memcpy(hash_input, h_seed, m_hash_length);

	uint8_t octetcounter[4];

	for (i = 0; i < (db_length / m_hash_length) -1; i++)
	{
		WRITE_UINT32(octetcounter, i);
		memcpy(hash_input + m_hash_length, octetcounter, 4);
		hash_func->init(hash_ctx);
	    hash_func->update(hash_ctx, m_hash_length + 4, hash_input);
	    hash_func->digest(hash_ctx, m_hash_length, t);

	    memxor(db + (m_hash_length * i), t , m_hash_length);
	}

	/* 4. Output the leading maskLen octets of T as the octet string mask. */
	// Done above where we copy T into dbmask.
	ret = 1;

cleanup:
	TMP_GMP_FREE(hash_input);
	TMP_GMP_FREE(t);

	return ret;
}

