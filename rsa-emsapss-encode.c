/* rsa-emsapss-encode.c

   Encoding primitive for PSS signatures.

   Copyright (C) 2015 Andrew James Lawrence

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdlib.h>
#include <string.h>
#include "pss.h"
#include "bignum.h"
#include "gmp-glue.h"
#include "nettle-internal.h"

int
emsapss_rsa_encode(void* hash_ctx, const struct nettle_hash *hash_func,
		           size_t salt_length, const uint8_t *salt,
			       size_t m_hash_length, const uint8_t *m_hash,
                   size_t embits, mpz_t em)
{
	int ret;

	/* 1.  If the length of M is greater than the input limitation for the
	       hash function (2^61 - 1 octets for SHA-1), output "message too
	       long" and stop. */
	/* 2.  Let mHash = Hash(M), an octet string of length hLen. */
	/* m_hash is an input to this function so we can skip the above two steps */
	const size_t emlen = (embits+7)/8;
    const size_t db_length = emlen - m_hash_length -1;

    TMP_GMP_DECL(h, uint8_t);
    TMP_GMP_DECL(m_prime, uint8_t);
    TMP_GMP_DECL(db, uint8_t);
    TMP_GMP_DECL(dbmask, uint8_t);
    TMP_GMP_DECL(em_prime, uint8_t);

	TMP_GMP_ALLOC(h, m_hash_length);
    TMP_GMP_ALLOC(m_prime, 8 + m_hash_length + salt_length);
    TMP_GMP_ALLOC(db, db_length);
    TMP_GMP_ALLOC(dbmask, db_length);
	TMP_GMP_ALLOC(em_prime, emlen);

	/* 3.  If emLen < hLen + sLen + 2, output "encoding error" and stop. */
    if (emlen < m_hash_length + salt_length +2)
    {
    	ret = 0;
    	goto cleanup;
    }

	/* 4.  Generate a random octet string salt of length sLen; if sLen = 0,
	       then salt is the empty string. */
    // We pass the salt as input to this function.

	/* 5.  Let M' = (0x)00 00 00 00 00 00 00 00 || mHash || salt;
           M' is an octet string of length 8 + hLen + sLen with eight
	       initial zero octets.  */
    // Set the first 8 octets of M' to 0
    memset(m_prime, 0, 8);

    // Copy mHash into M'
    memcpy(m_prime + 8, m_hash, m_hash_length);

    // Copy salt into M'
    memcpy(m_prime +8 + m_hash_length, salt, salt_length);

	/*  6.  Let H = Hash(M'), an octet string of length hLen. */
	hash_func->init(hash_ctx);
    hash_func->update(hash_ctx, 8 + m_hash_length + salt_length, m_prime);
    hash_func->digest(hash_ctx, m_hash_length, h);

	/*  7.  Generate an octet string PS consisting of emLen - sLen - hLen - 2
	        zero octets.  The length of PS may be 0. */
	/*  8.  Let DB = PS || 0x01 || salt; DB is an octet string of length
	        emLen - hLen - 1. */

	// Set the PS octets in DB to be 0
    memset(db, 0, emlen - salt_length - m_hash_length - 2);

	// Set the next octet to be 0x01
	db[emlen - salt_length - m_hash_length -2] = 0x01;

	// Copy the salt into DB
	memcpy(db + emlen - salt_length - m_hash_length -2,
		   salt, salt_length);

	/*   9.  Let dbMask = MGF(H, emLen - hLen - 1). */
	/*   10. Let maskedDB = DB \xor dbMask. */
	if (!_nettle_mgf1xor(hash_ctx, hash_func, m_hash_length, h, db_length, dbmask))
    {
    	ret = 0;
    	goto cleanup;
    }

	/*   11. Set the leftmost 8emLen - emBits bits of the leftmost octet in
	         maskedDB to zero. */
	db[0] &= (0xFF >> (unsigned int)(8*emlen-embits));

	/*   12. Let EM = maskedDB || H || 0xbc. */
	// We use a temporary variable EM' to compose the output
	// Copy maskeddb into EM'
	memcpy(em_prime, db, db_length);
	memcpy(em_prime + db_length, h, m_hash_length);
	em_prime[emlen -1] = 0xbc;

	/*   13. Output EM. */
	nettle_mpz_set_str_256_u(em, emlen, em_prime);
	ret = 1;

	// Clean up. Free temporary variables and return.
cleanup:
    TMP_GMP_FREE(h);
    TMP_GMP_FREE(m_prime);
    TMP_GMP_FREE(db);
    TMP_GMP_FREE(dbmask);
    TMP_GMP_FREE(em_prime);

	return ret;
}


