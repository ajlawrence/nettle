/* rsa-pss-verify.c

   PSS signatures.

   Copyright (C) 2015 Andrew James Lawrence

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include "rsa.h"
#include "pss.h"
#include "bignum.h"

int
rsa_pss_verify(const struct rsa_public_key *key,
		 void* hash_ctx, const struct nettle_hash *hash_func,
		 size_t salt_length,
		 size_t m_hash_length, const uint8_t *m_hash,
		 const mpz_t signature)
{
  int ret;
  mpz_t m;

  if (mpz_sgn(signature) <= 0)
  {
	  ret = 0;
	  goto cleanup;
  }

  mpz_init (m);
  mpz_powm(m, signature, key->e, key->n);

  size_t embits, emlen;
  embits = key->size*8;
  emlen = (embits + 7) / 8;
  if (emlen < mpz_sizeinbase(m,2))
  {
	 ret = 0;
	 goto cleanup;
  }

  /* Here M is still in integer representation.
     In the RFC it states M should be converted
     to an octet string. We do that inside the
     verify function */
  ret = emsapss_rsa_verify(hash_ctx, hash_func,
		                   salt_length,
		                   m_hash_length, m_hash,
                           embits, m);
cleanup:
  mpz_clear(m);

  return ret;
}
